// Теоретичні питання:
// 1. DOM це модель керування контентом на сторінці браузера;
// 2. innerText дозволяє змінювати тільки текстовий вміст елемента. 
//  За допомогою innerHTML додатково можна встроювати теги;
// 3. getElementsBy* або querySelector. querySelector є новішим та більш ефективнішим.

// Завдання:

const paragraph = document.querySelectorAll('p');
paragraph.forEach(el => el.style.background = '#ff0000');

const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentOptionslist = optionsList.parentElement;
console.log(parentOptionslist);

const childNodes = optionsList.children;
if (optionsList.hasChildNodes()) {
    for(let el of childNodes) {
        console.log(`nodeName: ${el.nodeName}, nodeType: ${el.nodeType}.`);
    }
}

const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';

const mainHeaderElements = document.querySelector('.main-header').children;
console.log(mainHeaderElements);
for (let el of mainHeaderElements) {
	el.classList.add('nav-item');
}
console.log(mainHeaderElements);

const sectionTitleElements = document.querySelectorAll('.section-title');
console.log(sectionTitleElements);

for (let el of sectionTitleElements) {
	el.classList.remove('section-title');
}
console.log(sectionTitleElements);
